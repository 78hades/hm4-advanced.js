const url = 'https://ajax.test-danit.com/api/swapi/films'; 

function fetchRequest(url) {
  return fetch(url).then((responseData) => {
    if (!responseData.ok) {
      throw new Error(`Error`);
    }
    return responseData.json();
  });
}
const container = document.querySelector('.container');

function filmDescription(data) {

  data.forEach((film) => {
    const filmContainer = document.createElement('div');
    const filmName = document.createElement('h2');
    filmName.textContent = `${film.episodeId}: ${film.name}`;

    const cast = document.createElement('p');
    cast.textContent = 'Characters: ';
    film.characters.forEach((url, index) => {
     fetchRequest(url)
        .then((character) => {
          cast.textContent += `${character.name}${index < film.characters.length - 1 ? ', ' : ''}`;
        })
        .catch((err) => {
          console.log(err);
        });
    });

    const filmAbout = document.createElement('p');
    filmAbout.textContent = `${film.openingCrawl}`;

    filmContainer.appendChild(filmName);
    filmContainer.appendChild(cast);
    filmContainer.appendChild(filmAbout);
    container.appendChild(filmContainer);
  });
}

fetchRequest(url)
  .then((data) => {
    filmDescription(data);
  })
  .catch((err) => {
    console.log(err);
  });